# Guía Repaso Parcial 4

1. [Productora](GRP4-Ej01.md)
2. [Colegio Profesional](GRP4-Ej02.md)
3. [(Parcial 2021) Empleos](GRP4-Ej03.md)
4. [(Parcial 2021) Oficina Legal](GRP4-Ej04.md)
5. [(Parcial 2021) Servicios Audiovisuales](GRP4-Ej05.md)
6. [(Parcial 2021) Videojuegos](GRP4-Ej06.md)
