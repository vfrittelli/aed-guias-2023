import os.path
import pickle
import random
import registro


def validar_mayor_que(limite, mensaje='Ingrese un numero:'):
    num = limite
    while num <= limite:
        num = int(input(mensaje))
        if num <= limite:
            print(f'Error el valor debe ser mayor a {limite}')
    return num


def validar_rango(inf, sup, mensaje='Ingrese un numero:'):
    num = inf - 1
    while num < inf or num > sup:
        num = int(input(mensaje))
        if num < inf or num > sup:
            print(f'Error el valor debe ser mayor o igual a {inf} y menor o igual a {sup}')
    return num


def menu():
    cadena = 'Menu de Opciones:\n' \
             '===========================================\n' \
             '1 - Cargar los datos en un arreglo \n' \
             '2 - Mostrar el arreglo creado  \n' \
             '3 - Buscar en el arreglo por descripcion \n' \
             '4 - Crear un archivo de registros \n' \
             '5 - Mostrar el archivo creado  \n' \
             '0 - Salir\n' \
             'Ingrese su opcion: '
    return int(input(cadena))


def add_in_order(vector, caso):
    izq, der = 0, len(vector) - 1
    pos = 0
    while izq <= der:
        med = (izq + der) // 2
        if vector[med].numero == caso.numero:
            pos = med
            break

        if caso.numero > vector[med].numero:
            izq = med + 1
        else:
            der = med - 1

    if izq > der:
        pos = izq

    vector[pos:pos] = [caso]


def cargar_casos(vector):
    fd = "casos.csv"
    m = open(fd, "rt")
    print('Creando el arreglo a partir del archivo', fd)

    line = m.readline()
    while line != "":
        tokens = line.split(",")
        num = int(tokens[0])
        des = tokens[1]
        mon = round(float(tokens[2]), 2)
        tip = int(tokens[3])
        tri = int(tokens[4])
        caso = registro.Caso(num, des, mon, tip, tri)
        add_in_order(vector, caso)
        line = m.readline()

    m.close()
    print("Hecho...")
    print()


def mostrar(vector, tribunal):
    print('Listado de Casos')
    print(registro.encabezado())
    for caso in vector:
        if caso.tribunal != tribunal:
            print(caso)


def buscar(vector, des):
    pos = -1
    for i in range(len(vector)):
        if vector[i].descripcion == des:
            pos = i
            break
    return pos


def generar_archivo(vector, x, archivo):
    m = open(archivo, 'wb')
    for caso in vector:
        if (caso.tipo == 3 or caso.tipo == 4) and caso.monto < x:
            pickle.dump(caso, m)
    m.close()


def mostrar_archivo(archivo):
    if not os.path.exists(archivo):
        print('No esiste el archivo!!!!')

    cant = 0
    m = open(archivo, 'rb')
    size = os.path.getsize(archivo)
    print(registro.encabezado())
    while m.tell() < size:
        caso = pickle.load(m)
        print(caso)
        cant += 1
    m.close()
    print(f'Se leyeron {cant} registros')


def principal():
    opcion = -1
    vector = []
    archivo = 'casos.dat'

    while opcion != 0:
        opcion = menu()

        if opcion == 1:
            cargar_casos(vector)

        elif len(vector) > 0:
            if opcion == 2:
                t = validar_rango(1, 10, 'Ingrese el tribunal a excluir: ')
                mostrar(vector, t)
                print()

            elif opcion == 3:
                des = input('Ingerse la descripcion a buscar: ')
                pos = buscar(vector, des)
                if pos != -1:
                    print(registro.encabezado())
                    print(vector[pos])
                else:
                    print('No se ha encontrado un caso con esa descripcion')
                print()

            elif opcion == 4:
                x = validar_mayor_que(0, 'Ingrese el monto maximo de caso: ')
                generar_archivo(vector, x, archivo)
                print("Hecho...")
                print()

            elif opcion == 5:
                mostrar_archivo(archivo)
                print()

        else:
            print('Primero debe ejecutar la opcion 1')


if __name__ == '__main__':
    principal()
