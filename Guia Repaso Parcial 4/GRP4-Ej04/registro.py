class Caso:
    def __init__(self, numero, descripcion, monto, tipo, tribunal):
        self.numero = numero
        self.descripcion = descripcion
        self.monto = monto
        self.tipo = tipo
        self.tribunal = tribunal

    def __str__(self):
        cadena = f'| {self.numero:<10} | {self.descripcion:<30} | ${self.monto:>10.2f} | {self.tipo:^4} ' \
                 f'| {self.tribunal:^8} |'
        return cadena


def encabezado():
    cadena = f'| {"Numero":<10} | {"Descripcion":<30} | {"Monto":^11} | {"Tipo":^4} | {"Tribunal":^8} |'
    return cadena
