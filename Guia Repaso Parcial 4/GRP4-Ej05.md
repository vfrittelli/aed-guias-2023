### (Parcial 2021) Servicios Audiovisuales
Una empresa de administración de servicios audiovisuales desea un programa para gestionar los datos de sus contenidos. Por cada contenido se tienen los siguientes datos: código (un número entero que se asigna como identificador), nombre del contenido, tipo de contenido (un número entero que va del 1 al 15 ambos valores incluidos), calificación (un entero que determina el nivel de violencia del contenido y que va del 0 al 12 ambos valores incluidos), y la cantidad de reproducciones (un valor entero que determina cual fue el total de reproducciones que ese contenido tuvo).

Se pide definir la clase Contenido con los atributos pedidos, y desarrollar un programa en Python controlado por menú de opciones que permite gestionar las siguientes tareas:

1. Cargar un arreglo de objetos con los datos de n contenidos de manera que en todo momento se mantenga ordenado por descripción. Para esto debe utilizar el algoritmo de inserción ordenada con búsqueda binaria (se considerará directamente incorrecta la solución basada en cargar el arreglo completo y ordenarlo al final, o aplicar el algoritmo de inserción ordenada, pero con búsqueda secuencial). Valide que el tipo de contenido y la calificación estén dentro de los valores descriptos y recuerde que estos son números enteros. Puede hacer la carga en forma manual, o puede generar los datos en forma automática (con valores aleatorios) (pero si hace carga manual, TODA la carga debe ser manual, y si la hace automática entonces TODA debe ser automática).

2. Mostrar el contenido del vector a razón de un objeto por línea, pero muestre solo los objetos cuya cantidad de reproducciones sea mayor a un valor c que se carga por teclado.

3. A partir del vector, generar una matriz que permita acumular el total de reproducciones que se realizaron por tipo de contenido y calificación (Un total de 195 acumuladores). Al finalizar la generación de la matriz muestre solo aquellos acumuladores con valores mayores a cero.

4. A partir del vector, generar un archivo binario con todos los contenidos cuyo tipo de contenido no sea ni 4, ni 8 ni 12 y cuya calificación sea mayor a un valor ingresado por teclado.

5. Mostrar el archivo generado en el punto anterior a razón de un registro por línea. Al final del listado indique cual es el total general reproducciones de todos esos registros mostrados.
