class Empleo:
    def __init__(self, num, desc, mon, ciud, te):
        self.numero = num
        self.descripcion = desc
        self.monto = mon
        self.ciudad = ciud
        self.tipo = te

    def __str__(self):
        cad = "Numero: " + str(self.numero)
        cad += " - Descripcion: " + self.descripcion
        cad += " - Monto: " + str(self.monto)
        cad += " - Ciudad: " + str(self.ciudad)
        cad += " - Tipo: " + str(self.tipo)
        return cad
