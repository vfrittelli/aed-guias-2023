import empleo
import random
import pickle
import os.path


def validate(inf):
    n = inf
    while n <= inf:
        n = int(input('Valor (mayor a ' + str(inf) + '): '))
        if n <= inf:
            print('Error: se pidio mayor a', inf, '... cargue de nuevo...')
    return n


def validate_range(mn=0, mx=29):
    cod = int(input('Ingrese valor (>= ' + str(mn) + ' y <= ' + str(mx) + '): '))
    while cod < mn or cod > mx:
        cod = int(input('Error... era >=' + str(mn) + ' y <=' + str(mx) + '). De nuevo: '))
    return cod


def add_in_order(v, e):
    n = len(v)
    izq, der = 0, n - 1
    pos = n
    while izq <= der:
        c = (izq + der) // 2
        if e.numero == v[c].numero:
            pos = c
            break

        elif e.numero < v[c].numero:
            der = c - 1
        else:
            izq = c + 1

    if izq > der:
        pos = izq

    v[pos:pos] = [e]


def read(v):
    fd = "empleos.csv"
    m = open(fd, "rt")
    print('Creando el arreglo a partir del archivo', fd)
    while True:
        line = m.readline()
        if line == "":
            break

        tokens = line.split(",")
        num = int(tokens[0])
        des = tokens[1]
        imp = round(float(tokens[2]), 2)
        ciu = int(tokens[3])
        tip = int(tokens[4])

        e = empleo.Empleo(num, des, imp, ciu, tip)
        add_in_order(v, e)
    m.close()
    print("Hecho...")


def display(v):
    print("Ingrese número inicial de ciudad (entre 0 y 29):")
    c1 = validate_range(0, 29)

    print("Ingrese número final de ciudad (entre 0 y 29):")
    c2 = validate_range(0, 29)

    n = len(v)
    print('Empleos entre las ciudades', c1, "y", c2)
    for i in range(n):
        if c1 <= v[i].ciudad <= c2:
            print(v[i])


def count(v):
    print("Ingrese valor inicial de conteo (no negativo, por favor):")
    a = validate(-1)

    print("Ingrese valor final de conteo (mayor que", a, "por favor):")
    b = validate(a)

    mat = [[0] * 20 for i in range(30)]
    n = len(v)
    for i in range(n):
        f = v[i].ciudad
        c = v[i].tipo
        mat[f][c] += 1

    print('Cantidades de empleos en el rango pedido:')
    for f in range(30):
        for c in range(20):
            if a <= mat[f][c] <= b:
                print('Ciudad', f, 'Tipo', c, 'Cantidad de empleos:', mat[f][c])


def save_file(v, fd):
    print('Monto de referencia (no negativo, por favor):')
    p = validate(-1)

    m = open(fd, "wb")
    n = len(v)
    for i in range(n):
        if v[i].monto > p and v[i].tipo <= 15:
            pickle.dump(v[i], m)
    m.close()

    print("Archivo creado...")


def display_file(v, fd):
    if not os.path.exists(fd):
        print("No existe el archivo", fd)
        print("Debe crearlo desde la opción 4...")
        return

    c = 0
    t = os.path.getsize(fd)
    m = open(fd, "rb")
    while m.tell() < t:
        r = pickle.load(m)
        print(r)
        c += 1
    m.close()

    print("Total de registros listados:", c)


def main():
    # inserción ordenada... comenzar con ARREGLO VACÍO!!!
    v = []

    # nombre del archivo a generar...
    fd = "empleos.dat"

    opc = 0
    while opc != 6:
        print('\nMenu de opciones:')
        print('1. Cargar empleos')
        print('2. Mostrar empleos en rango de ciudades')
        print('3. Contar empleos por ciudad y tipo')
        print('4. Generar archivo (montos mayores a "p")')
        print('5. Mostrar archivo (con conteo de registros)')
        print('6. Salir')

        opc = int(input('Ingrese su eleccion: '))

        if opc == 1:
            read(v)

        elif opc == 2:
            if len(v) != 0:
                display(v)
            else:
                print('No  hay datos cargados...')

        elif opc == 3:
            if len(v) != 0:
                count(v)
            else:
                print('No  hay datos cargados...')

        elif opc == 4:
            if len(v) != 0:
                save_file(v, fd)
            else:
                print('No  hay datos cargados...')

        elif opc == 5:
            display_file(v, fd)

        elif opc == 6:
            print("--- Programa finalizado ---")


if __name__ == '__main__':
    main()
