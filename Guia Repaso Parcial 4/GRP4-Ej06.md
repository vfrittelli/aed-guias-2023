### (Parcial 2021) Videojuegos
Una empresa de venta de videojuegos mantiene información sobre los distintos juegos que tiene a la venta. Por cada juego se registran los datos siguientes: número de identificación (un entero), nombre del juego (una cadena), cantidad en stock (puede ser cero), precio de venta, pais de origen del juego (un valor entre 0 y 29 incluidos, por ejemplo: 0: Argentina, 1: USA, etc.) y tipo de juego (un número entero entre 0 y 14 incluidos, para indicar (por ejemplo): 0: de acción, 1: de deportes, etc.).

Se pide definir la clase Juego con los atributos que se indicaron, y un programa completo con menú de opciones para hacer lo siguiente:

1. Cargar los datos de n objetos de tipo Juego en un arreglo de registros (cargue n por teclado). Valide que el stock y el pais de origen estén dentro de los valores descriptos. Puede cargar los datos manualmente, o puede generarlos aleatoriamente (pero si hace carga manual, TODA la carga debe ser manual, y si la hace automática entonces TODA debe ser automática). El arreglo debe crearse de forma que siempre quede ordenado de menor a mayor, según el nombre de los juegos, y para hacer esto debe aplicar el algoritmo de inserción ordenada con búsqueda binaria. Se considerará directamente incorrecta la solución basada en cargar el arreglo completo y ordenarlo al final, o aplicar el algoritmo de inserción ordenada pero con búsqueda secuencial.

2. Mostrar el arreglo creado en el punto 1, a razón de un objeto por línea, pero muestre solo los datos de los objetos cuyo pais de origen sea igual a p (cargue p por teclado).

3. Usando el arreglo creado en el punto 1, determine la cantidad de juegos de cada posible pais de origen por cada posible tipo (o sea, 30 * 15 contadores en una matriz de conteo). Muestre solo los contadores cuyo valor final esté entre 0 y un valor x que se carga por teclado.

4. A partir del arreglo, crear un archivo binario de registros en el cual se copien los datos de todos los objetos cuya cantidad en stock no sea cero, y su pais de origen no sea ni 0 ni 1.

5. Mostrar el archivo creado en el punto 4, a razón de un registro por línea en la pantalla. Al final de listado muestre una línea adicional en la que se indique el precio promedio de todos los juegos mostrados.
