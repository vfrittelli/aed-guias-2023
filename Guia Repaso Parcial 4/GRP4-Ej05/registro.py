class Contenido:
    def __init__(self, cod, nom, tipo,calif, repr):
        self.codigo = cod
        self.nombre = nom
        self.tipo = tipo
        self.calificacion = calif
        self.reproducciones = repr

    def __str__(self):
        cad = 'Codigo: {} - Nombre: {} - Tipo: {} - Calificacion: {} - Reproducciones: {}'
        return cad.format(self.codigo, self.nombre, self.tipo, self.calificacion, self.reproducciones)