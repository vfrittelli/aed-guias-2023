import os.path
import pickle
import random

from registro import *


def mostrar_menu():
    print('\nMENU DE OPCIONES')
    print('1. Cargar')
    print('2. Mostrar')
    print('3. Generar matriz')
    print('4. Generar archivo')
    print('5. Mostrar archivo')
    print('0. Salir')
    opcion = int(input('Ingrese opcion: '))
    return opcion


def validar_mayor_que(lim, mensaje):
    num = int(input(mensaje))
    while num <= lim:
        num = int(input('Error! ' + mensaje))
    return num


def add_in_order(v, reg):
    izq, der = 0, len(v) - 1
    pos = 0
    while izq <= der:
        c = (izq + der) // 2
        if v[c].nombre == reg.nombre:
            pos = c
            break
        if reg.nombre < v[c].nombre:
            der = c - 1
        else:
            izq = c + 1
    if izq > der:
        pos = izq
    v[pos:pos] = [reg]


def cargar_ordenado(n):
    v = []
    nombres = ('Video', 'Humor', 'Musica', 'TikTok')
    for i in range(n):
        codigo = random.randint(100, 999)
        nombre = random.choice(nombres) + str(i)
        tipo = random.randint(1, 15)
        calificacion = random.randint(0, 12)
        reproducciones = random.randint(1000, 9999)
        reg = Contenido(codigo, nombre, tipo, calificacion, reproducciones)
        add_in_order(v, reg)
    return v


def mostrar_vector(v, c):
    for i in range(len(v)):
        if v[i].reproducciones > c:
            print(v[i])


def generar_matriz(v):
    m = [[0] * 13 for f in range(15)]
    for i in range(len(v)):
        fila = v[i].tipo - 1
        col = v[i].calificacion
        m[fila][col] += v[i].reproducciones
    return m


def mostrar_matriz(m):
    for f in range(len(m)):
        for c in range(len(m[f])):
            if m[f][c] > 0:
                print('Tipo', f + 1, 'Calificacion', c, ': ', m[f][c], 'reproducciones')


def generar_archivo(v, fd, calif):
    m = open(fd, 'wb')
    for i in range(len(v)):
        if v[i].tipo != 4 and v[i].tipo != 8 and v[i].tipo != 12 and v[i].calificacion > calif:
            pickle.dump(v[i], m)
    m.close()
    print('Archivo', fd, 'generado con éxito')


def mostrar_archivo(fd):
    tot = 0
    if os.path.exists(fd) == False:
        print('El archivo', fd, 'no existe')
        return
    m = open(fd, 'rb')
    tam = os.path.getsize(fd)
    while m.tell() < tam:
        reg = pickle.load(m)
        tot += reg.reproducciones
        print(reg)
    m.close()
    print('Total general de reproducciones:', tot)


def principal():
    v = list()
    fd = 'contenidos.dat'
    opcion = -1
    while opcion != 0:
        opcion = mostrar_menu()
        if opcion == 1:
            n = validar_mayor_que(0, 'Cantidad de contenidos: ')
            v = cargar_ordenado(n)
        elif len(v) == 0:
            print('El vector no está cargado')
        elif opcion == 2:
            c = int(input('Ingrese cantidad a comparar: '))
            mostrar_vector(v, c)
        elif opcion == 3:
            m = generar_matriz(v)
            mostrar_matriz(m)
        elif opcion == 4:
            calif = int(input('Ingrese calificación a comparar: '))
            generar_archivo(v, fd, calif)
        elif opcion == 5:
            mostrar_archivo(fd)


if __name__ == '__main__':
    principal()
