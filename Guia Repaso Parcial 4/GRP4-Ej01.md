### Productora
Una empresa de producciones cinematográficas mantiene información sobre las distintas películas que tiene en desarrollo. Por cada película se registran los datos siguientes: número de identificación de la película (un número entero), título (una cadena), importe invertido en su producción, tipo de película (un valor entre 0 y 9 incluidos, de la forma 0: acción, 1: comedia, 2: drama, etc.) y un número para identificar el pais de origen de la película (un número entero entre 0 y 19 incluidos).

Se pide definir una clase Pelicula con los atributos que se indicaron, y un programa completo con menú de opciones para hacer lo siguiente:

1. Cargar los datos de n objetos de tipo Pelicula en un arreglo de objetos (cargue n por teclado). Puede cargar los datos manualmente, o puede generarlos aleatoriamente. El arreglo debe crearse de forma que siempre quede ordenado de menor a mayor, según el título de las películas.

2. Mostrar el arreglo creado en el punto 1, a razón de un objeto por línea.

3. Buscar en el arreglo creado en el punto 1 un objeto en el cual el titulo de la película sea igual a nom (cargar nom por teclado). Si existe, mostrar por pantalla los datos, pero previamente modificar el importe con un valor imp (cargar imp por teclado). Si no existe, informar con un mensaje. La búsqueda debe detenerse al encontrar el primer objeto que coincida con el titulo.

4. A partir del arreglo, crear un archivo binario de registros en el cual se copien los datos de todas las películas cuyo país de origen no sea el 10 y cuyo importe invertido sea menor a un valor x que se carga por teclado.

5. Mostrar el archivo creado en el punto anterior, a razón de un registro por línea en la pantalla.

6. Buscar en el arreglo creado en el punto 1 un objeto en el cual el número de identificación de la película sea igual a num (cargar num por teclado). Si existe, mostrar por pantalla todos los datos de ese objeto. Si no existe, informar con un mensaje. La búsqueda debe detenerse al encontrar el primer objeto que coincida con el patrón pedido.

7. Usando el arreglo creado en el punto 1, determine la cantidad de películas de cada posible tipo por cada posible país de origen (o sea, 10 * 20 = 200 contadores en una matriz de conteo). Muestre sólo los resultados que sean diferentes de 0.
