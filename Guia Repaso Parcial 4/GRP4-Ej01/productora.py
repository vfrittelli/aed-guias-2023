import random


class Pelicula:
    def __init__(self, numero, titulo, importe, tipo_pelicula, pais_origen):
        self.numero = numero
        self.titulo = titulo
        self.importe = importe
        self.tipo_pelicula = tipo_pelicula
        self.pais_origen = pais_origen

    def __str__(self):
        cad = '| {:<8} | {:<30} | {:>10.2f} | {:^10} | {:^10} |\n'
        cad += '{}\n'.format('-' * 84)
        return cad.format(self.numero, self.titulo, self.importe, self.tipo_pelicula, self.pais_origen)


def encabezado():
    cad = '{}\n'.format('-' * 84)
    cad += '| {:<8} | {:<30} | {:<10} | {:^10} | {:^10} |\n'
    cad += '{}\n'.format('-' * 84)
    return cad.format('Numero', 'Titulo', 'Importe', 'T Pelicula', 'Pais Orig')


def crear_titulo():
    tokens = ("Accion", "Drama", "Romance", "Comedia", "Terror", "SciFi", "Aventuras", "Fantasia")
    titulo = random.choice(tokens)
    return titulo
