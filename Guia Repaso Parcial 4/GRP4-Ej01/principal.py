import os
import pickle
import random

import productora
import validadores
from productora import Pelicula


def menu():
    cad = 'Menu de Opciones\n' \
          '==============================================\n' \
          '1 ----- Cargar Vector de Peliculas\n' \
          '2 ----- Mostrar Vector de Peliculas\n' \
          '3 ----- Buscar Pelicula por titulo\n' \
          '4 ----- Crear Archivo de Peliculas con filtro\n' \
          '5 ----- Mostrar Archivo de Peliculas\n' \
          '6 ----- Buscar Pelicula por numero de identificacion\n' \
          '7 ----- Generar matriz de conteo\n' \
          '0 ----- Salir\n' \
          'Ingrese su opcion: '
    return int(input(cad))


def add_in_order(vector, pelicula):
    izq, der = 0, len(vector) - 1
    pos = 0

    while izq <= der:
        med = (izq + der) // 2
        if vector[med].titulo == pelicula.titulo:
            pos = med
            break

        if pelicula.titulo < vector[med].titulo:
            der = med - 1
        else:
            izq = med + 1

    if izq > der:
        pos = izq

    vector[pos:pos] = [pelicula]


def cargar_peliculas(vector, n):
    for i in range(n):
        numero = random.randint(1, 1500)
        titulo = productora.crear_titulo() + " " + str(i)
        importe = random.uniform(1500, 15000)
        tipo_pelicula = random.randrange(10)
        pais_origen = random.randrange(20)
        pelicula = Pelicula(numero, titulo, importe, tipo_pelicula, pais_origen)
        add_in_order(vector, pelicula)


def mostrar_vector(vector):
    print(productora.encabezado(), end='')
    for pelicula in vector:
        print(pelicula, end='')


def buscar_por_titulo(vector, titulo):
    izq, der = 0, len(vector) - 1
    pos = -1

    while izq <= der:
        med = (izq + der) // 2
        if vector[med].titulo == titulo:
            pos = med
            break

        if titulo < vector[med].titulo:
            der = med - 1
        else:
            izq = med + 1

    return pos


def crear_archivo(vector, importe, nombre_archivo):
    m = open(nombre_archivo, 'wb')
    for pelicula in vector:
        if pelicula.tipo_pelicula != 10 and pelicula.importe < importe:
            pickle.dump(pelicula, m)
    m.close()


def leer_archivo(nombre_archivo):
    if not os.path.exists(nombre_archivo):
        print('No existe un archivo llamado {}'.format(nombre_archivo))
        return

    m = open(nombre_archivo, 'rb')
    size = os.path.getsize(nombre_archivo)
    print(productora.encabezado(), end='')
    while m.tell() < size:
        pelicula = pickle.load(m)
        print(pelicula, end='')
    m.close()


def buscar_por_numero(vector, num):
    pos = -1
    for i in range(len(vector)):
        if vector[i].numero == num:
            pos = i
            break
    return pos


def crear_matriz_conteo(vector):
    matriz = [[0] * 20 for i in range(10)]
    for pelicula in vector:
        f = pelicula.tipo_pelicula
        c = pelicula.pais_origen
        matriz[f][c] += 1
    return matriz


def mostrar_matriz(matriz):
    cad = 'Existen {:>5} peliculas para el tipo de pelicula {:>5} y pais de origen {:>5}'
    for f in range(len(matriz)):
        for c in range(len(matriz[f])):
            if matriz[f][c] > 0:
                print(cad.format(f, c, matriz[f][c]))


def principal():
    peliculas = []
    nombre_archivo = 'peliculas.dat'
    opcion = -1

    while opcion != 0:
        opcion = menu()
        if opcion == 1:
            n = validadores.validar_mayor_que(0, 'Ingrese la cantidad de peliculas a cargar: ')
            cargar_peliculas(peliculas, n)

        elif len(peliculas) > 0:
            if opcion == 2:
                mostrar_vector(peliculas)

            elif opcion == 3:
                nom = input('Ingrese el titulo de la pelicula: ')
                pos = buscar_por_titulo(peliculas, nom)
                if pos != -1:
                    imp = float(validadores.validar_mayor_que(0, 'Ingrese el nuevo importe de la pelicula: '))
                    peliculas[pos].importe = imp
                    print(peliculas[pos])
                else:
                    print('No existe una pelicula con el titulo {}'.format(nom))

            elif opcion == 4:
                imp = float(validadores.validar_mayor_que(0, 'Ingrese en importe minimo de la pelicula: '))
                crear_archivo(peliculas, imp, nombre_archivo)

            elif opcion == 5:
                leer_archivo(nombre_archivo)
            elif opcion == 6:
                num = int(input('Ingrese el numero de identificacion de la pelicula a buscar: '))
                pos = buscar_por_numero(peliculas, num)
                if pos != -1:
                    print(peliculas[pos])
                else:
                    print('No existe una pelicula con el numero {}'.format(num))

            elif opcion == 7:
                matriz = crear_matriz_conteo(peliculas)
                mostrar_matriz(matriz)
        else:
            print('Primero debe cargar el arreglo con peliculas a procesar')


if __name__ == '__main__':
    principal()
