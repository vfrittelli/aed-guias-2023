import os
import pickle
import random

import colegio
import validadores
from colegio import Profesional


def menu():
    cad = 'Menu de Opciones\n' \
          '==============================================\n' \
          '1 ----- Cargar Vector de Profesionales\n' \
          '2 ----- Mostrar Vector de Profesionales\n' \
          '3 ----- Buscar Profesional por dni\n' \
          '4 ----- Crear Archivo de Profesionales con filtro\n' \
          '5 ----- Mostrar Archivo de Profesionales\n' \
          '6 ----- Buscar Profesional por nombre\n' \
          '7 ----- Generar matriz de conteo\n' \
          '0 ----- Salir\n' \
          'Ingrese su opcion: '
    return int(input(cad))


def add_in_order(vector, profesional):
    izq, der = 0, len(vector) - 1
    pos = 0

    while izq <= der:
        med = (izq + der) // 2
        if vector[med].dni == profesional.dni:
            pos = med
            break

        if profesional.dni < vector[med].dni:
            der = med - 1
        else:
            izq = med + 1

    if izq > der:
        pos = izq

    vector[pos:pos] = [profesional]


def cargar_profesionales(vector, n):
    for i in range(n):
        dni = colegio.crear_dni()
        nombre = colegio.crear_nombre()
        importe = random.uniform(100, 2500)
        tipo_afiliacion = random.randrange(5)
        tipo_trabajo = random.randrange(15)
        profesional = Profesional(dni, nombre, importe, tipo_afiliacion, tipo_trabajo)
        add_in_order(vector, profesional)


def mostrar_vector(vector):
    print(colegio.encabezado(), end='')
    for profesional in vector:
        print(profesional)


def buscar_por_dni(vector, dni):
    izq, der = 0, len(vector) - 1
    pos = -1

    while izq <= der:
        med = (izq + der) // 2
        if vector[med].dni == dni:
            pos = med
            break

        if dni < vector[med].dni:
            der = med - 1
        else:
            izq = med + 1

    return pos


def crear_archivo(vector, importe, nombre_archivo):
    m = open(nombre_archivo, 'wb')
    for profesional in vector:
        if (2 < profesional.tipo_trabajo <= 5) and profesional.importe_cuota > importe:
            pickle.dump(profesional, m)
            print(profesional.tipo_trabajo)
    m.close()


def leer_archivo(nombre_archivo):
    if not os.path.exists(nombre_archivo):
        print('No existe un archivo llamado {}'.format(nombre_archivo))
        return

    m = open(nombre_archivo, 'rb')
    size = os.path.getsize(nombre_archivo)
    print(colegio.encabezado(), end='')
    while m.tell() < size:
        p = pickle.load(m)
        print(p)
    m.close()


def buscar_por_nombre(vector, nom):
    pos = -1
    for i in range(len(vector)):
        if vector[i].nombre == nom:
            pos = i
            break
    return pos


def crear_matriz_conteo(vector):
    matriz = [[0] * 15 for i in range(5)]
    for profesional in vector:
        f = profesional.tipo_afiliacion
        c = profesional.tipo_trabajo
        matriz[f][c] += 1
    return matriz


def mostrar_matriz(matriz):
    cad = 'Existen {:>5} profesionales para el tipo de afiliacion {:>5} y tipo de trabajo {:>5}'
    for f in range(len(matriz)):
        for c in range(len(matriz[f])):
            if matriz[f][c] > 0:
                print(cad.format(f, c, matriz[f][c]))


def principal():
    profesionales = []
    nombre_archivo = 'profesionales.dat'
    opcion = -1

    while opcion != 0:
        opcion = menu()
        if opcion == 1:
            n = validadores.validar_mayor_que(0, 'Ingrese la cantidad de profesionales a cargar: ')
            cargar_profesionales(profesionales, n)

        elif len(profesionales) > 0:
            if opcion == 2:
                mostrar_vector(profesionales)

            elif opcion == 3:
                dni = input('Ingrese el dni del profesional a buscar: ')
                pos = buscar_por_dni(profesionales, dni)
                if pos != -1:
                    imp = float(validadores.validar_mayor_que(0, 'Ingrese el importe de la cuota: '))
                    print(profesionales[pos])
                    if profesionales[pos].importe_cuota < imp:
                        print('El importe de la cuota esta desactualizado')
                else:
                    print('No existe un profesional con el dni {}'.format(dni))

            elif opcion == 4:
                imp = float(validadores.validar_mayor_que(0, 'Ingrese en importe: '))
                crear_archivo(profesionales, imp, nombre_archivo)

            elif opcion == 5:
                leer_archivo(nombre_archivo)

            elif opcion == 6:
                nom = input('Ingrese el nombre del profesional a buscar: ')
                pos = buscar_por_nombre(profesionales, nom)
                if pos != -1:
                    print(profesionales[pos])
                else:
                    print('No existe un profesional con el nombre {}'.format(nom))

            elif opcion == 7:
                matriz = crear_matriz_conteo(profesionales)
                mostrar_matriz(matriz)
            else:
                print('Primero debe cargar el arreglo con los profesionales a procesar')


if __name__ == '__main__':
    principal()
