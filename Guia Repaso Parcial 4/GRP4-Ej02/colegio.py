import random


class Profesional:
    def __init__(self, dni, nombre, importe_cuota, tipo_afiliacion, tipo_trabajo):
        self.dni = dni
        self.nombre = nombre
        self.importe_cuota = importe_cuota
        self.tipo_afiliacion = tipo_afiliacion
        self.tipo_trabajo = tipo_trabajo

    def __str__(self):
        cad = '| {:<10} | {:<30} | {:>10.2f} | {:^10} | {:^10} |'
        return cad.format(self.dni, self.nombre, self.importe_cuota, self.tipo_afiliacion, self.tipo_trabajo)


def encabezado():
    cad = '{}\n'.format('-' * 86)
    cad += '| {:^10} | {:<30} | {:<10} | {:^10} | {:^10} |\n'
    return cad.format('Dni', 'Nombre', 'Importe', 'Tpo Afilic', 'Tpo Trab')


def crear_dni():
    dni = '{}.{}.{}'
    return dni.format(random.randint(1, 99), random.randint(100, 999), random.randint(100, 999))


def crear_nombre():
    nom = '{} {}'
    nombres = ["Kato", "Cullen", "Oscar", "Lance", "Phyllis", "Hilel", "Joshua", "Peter", "Conan", "Phoebe"]
    apellidos = ["Chaney", "Odom", "Jackson", "Levy", "Molina", "Buckner", "Norman", "Townsend", "Hutchinson", "Hood"]
    return nom.format(random.choice(nombres), random.choice(apellidos))
