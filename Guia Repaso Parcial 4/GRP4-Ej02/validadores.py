def validar_mayor_que(minimo, mensaje='Cargar un numero: '):
    numero = minimo
    while numero <= minimo:
        numero = int(input(mensaje))
        if numero <= minimo:
            print('Error!!! El valor debe ser mayor a {}'.format(minimo))
    return numero
