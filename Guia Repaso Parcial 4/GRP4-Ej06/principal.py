import os.path
import pickle

import videojuego


def menu():
    print("1 _ Cargar juegos")
    print("2 _ Mostrar juegos de un pais")
    print("3 _ Cantidad por tipo y pais")
    print("4 _ Generar archivo")
    print("5 _ Mostrar archivo")
    print("6 _ Salir")
    return int(input("Ingrese opción: "))


def add_in_order(vector, elemento_nuevo):
    n = len(vector)
    izq = 0
    der = n - 1

    # mientras no se crucen
    while izq <= der:
        c = (izq + der) // 2
        if vector[c].nombre == elemento_nuevo.nombre:
            pos = c
            break
        elif elemento_nuevo.nombre < vector[c].nombre:
            der = c - 1
        else:
            izq = c + 1

    # Si no se encontró, tomo posición izquierda
    if der < izq:
        pos = izq

    vector[pos:pos] = [elemento_nuevo]


def cargar_juegos(v, n):
    for i in range(n):
        juego = videojuego.cargar_juego()
        add_in_order(v, juego)


def mostrar_juegos(v, pais):
    for juego in v:
        if juego.pais == pais:
            print(juego)


def contar_por_pais_tipo(v):
    cant = [[0] * 15 for i in range(30)]
    for juego in v:
        pais = juego.pais
        tipo = juego.tipo
        cant[pais][tipo] += 1
    return cant


def mostrar_cantidades(cantidades, x):
    for i in range(len(cantidades)):
        for j in range(len(cantidades[i])):
            valor = cantidades[i][j]
            if 0 < valor < x:
                print(f"Hay {valor} juegos del pais {i} y tipo {j}")


def generar_archivo(nombre, v):
    archivo = open(nombre, "wb")
    for juego in v:
        if juego.pais not in (0, 1) and juego.stock > 0:
            pickle.dump(juego, archivo)
    archivo.close()


def mostrar_archivo(nombre):
    if not os.path.exists(nombre):
        print("El archivo no existe")
    else:
        size = os.path.getsize(nombre)
        archivo = open(nombre, "rb")

        suma = cantidad = 0
        while archivo.tell() < size:
            juego = pickle.load(archivo)
            print(juego)
            suma += juego.precio
            cantidad += 1

        if cantidad > 0:
            promedio = suma/cantidad
            print("El precio promedio es:", promedio)
        else:
            print("No hay registros en el archivo")

        archivo.close()


def principal():
    fd = "juegos.dat"
    v = []
    op = -1
    while op != 6:
        op = menu()
        if op == 1:
            n = int(input("Ingrese cantidad de elementos: "))
            v = []
            cargar_juegos(v, n)
            print()

        elif op == 2:
            if len(v) != 0:
                p = int(input("Ingrese país a mostrar: "))
                mostrar_juegos(v, p)
            else:
                print("Cargue primero el arreglo...")
            print()

        elif op == 3:
            if len(v) != 0:
                cantidades = contar_por_pais_tipo(v)
                x = int(input("Ingrese valor para mostrar: "))
                mostrar_cantidades(cantidades, x)
            else:
                print("Cargue primero el arreglo...")
            print()

        elif op == 4:
            if len(v) != 0:
                generar_archivo(fd, v)
            else:
                print("Cargue primero el arreglo...")
            print()

        elif op == 5:
            mostrar_archivo(fd)
            print()


if __name__ == '__main__':
    principal()
