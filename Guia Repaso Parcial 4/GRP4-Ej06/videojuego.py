import random

SAGAS = "Final Fantasy", "Ys", "Uncharted", "Silent Hill", "Resident Evil"


class Juego:
    def __init__(self, numero, nombre, stock, precio, pais, tipo):
        self.numero = numero
        self.nombre = nombre
        self.stock = stock
        self.precio = precio
        self.pais = pais
        self.tipo = tipo

    def __str__(self):
        res = "Numero: {} - Nombre: {} - Stock: {} - Precio: {} - Pais: {} - Tipo: {}"
        return res.format(self.numero, self.nombre, self.stock, self.precio, self.pais, self.tipo)


def cargar_juego():
    numero = random.randint(1, 1000)
    nombre = random.choice(SAGAS) + " " + str(random.randint(1, 10))
    stock = random.randint(1, 100)
    precio = random.randint(100, 20000)
    pais = random.randint(0, 29)
    tipo = random.randint(0, 14)
    return Juego(numero, nombre, stock, precio, pais, tipo)
