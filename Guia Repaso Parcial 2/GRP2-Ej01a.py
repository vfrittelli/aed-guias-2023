def es_vocal(caracter):
    return caracter in "aeiouAEIOUáéíóúÁÉÍÓÚ"


def calcular_porcentaje(cantidad, total):
    porcentaje = 0
    if total > 0:
        porcentaje = cantidad * 100 / total
    return porcentaje


def principal():
    print("Primer Ejericio Repaso Parcial 2")
    print("=" * 60)

    texto = input("Ingrese el texto a procesar (debe finalizar con punto): ")
    texto = texto.lower()

    cont_letras = cont_vocales = 0
    cont_palabras = cont_palabras_fin_3_letra = cont_pal_2_vocales = cont_palabras_empieza_conso = 0
    tercer_car_pri_palabra = anterior = ""
    pos_ult_vocal = 0
    empieza_consonante = False

    for caracter in texto:
        if caracter != " " and caracter != ".":
            cont_letras += 1
            if cont_palabras == 0 and cont_letras == 3:
                tercer_car_pri_palabra = caracter

            if es_vocal(caracter):
                cont_vocales += 1
                pos_ult_vocal = cont_letras

            if cont_letras == 1 and not es_vocal(caracter):
                empieza_consonante = True

        else:
            cont_palabras += 1
            medio = cont_letras // 2

            if cont_palabras > 1 and anterior == tercer_car_pri_palabra:
                cont_palabras_fin_3_letra += 1

            if cont_letras >= 5 and cont_vocales > 1 and pos_ult_vocal > medio:
                cont_pal_2_vocales += 1

            if cont_letras % 2 != 0 and empieza_consonante:
                cont_palabras_empieza_conso += 1

            cont_letras = 0
            cont_vocales = 0
            empieza_consonante = False
        anterior = caracter

    print("La cantidad de palabras que terminan con el tercer caracter de la primera letra son:",
          cont_palabras_fin_3_letra)
    print("La cantidad de palabras que tienen mas de 5 letras y vocales en la segunda mitad d ela palabra son:",
          cont_pal_2_vocales)
    print("La cantidad de palabras de longitud impar que comienzan con consonante son:", cont_palabras_empieza_conso)

    porcentaje = calcular_porcentaje(cont_palabras_empieza_conso, cont_palabras)
    print("y representan un", round(porcentaje, 2), "% sobre el total de palabras del texto")


if __name__ == '__main__':
    principal()