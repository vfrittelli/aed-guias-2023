def calcular_porcentaje(cantidad, total):
    porcentaje = 0
    if total > 0:
        porcentaje = cantidad * 100 / total
    return porcentaje


def es_digito(caracter):
    return caracter in "0123456789"


def principal():
    print("Ejercicio Repaso Parcial")
    print("*" * 60)

    texto = input("Ingrese el texto que desea procesar (debe terminar con punto): ")
    texto = texto.lower()

    cont_caracteres = cont_digitos = cont_letras = cont_palabras_mas_digitos = 0
    cont_palabras = cont_palabras_fin_s = cont_palabras_long_impar = cont_palabras_contiene_pl = 0
    pos_letra_l = 0
    anterior = ""
    tiene_l = tiene_pl = tiene_letra = False

    for caracter in texto:
        if caracter != " " and caracter != ".":
            cont_caracteres += 1

            if not tiene_l and caracter == "l":
                pos_letra_l = cont_caracteres
                tiene_l = True

            if cont_caracteres >= 3:
                if anterior == "p" and caracter == "l":
                    tiene_pl = True

            if es_digito(caracter):
                cont_digitos += 1
            else:
                tiene_letra += 1

        else:
            cont_palabras += 1
            medio = cont_caracteres // 2

            if cont_caracteres % 2 != 0:
                cont_palabras_long_impar += 1

            if pos_letra_l < medio and anterior == "s":
                cont_palabras_fin_s += 1

            if tiene_pl:
                cont_palabras_contiene_pl += 1

            if 1 <= cont_letras < cont_digitos:
                cont_palabras_mas_digitos += 1

            tiene_l = False
            tiene_pl = False
            cont_caracteres = 0
        anterior = caracter

    print("La cantidad de palabras que tienen una \"l\" en la primera mitad y termina en \"s\" son:",
          cont_palabras_fin_s)

    porcentaje = calcular_porcentaje(cont_palabras_long_impar, cont_palabras)
    print("La cantidad de palabras de longitud impar son:", cont_palabras_long_impar, "y representan el",
          round(porcentaje, 2), "% sobre el total de palabras del texto")

    print("La cantidad de palabras que contienen la secuencia \"pl\" a partir del tercer caracter son:",
          cont_palabras_contiene_pl)

    print("La cantidad de palabras con mas digitos que letras son:", cont_palabras_mas_digitos)


if __name__ == '__main__':
    principal()
