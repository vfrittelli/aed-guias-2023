def calcular_porcentaje(muestra, total):
    """
    Calcula el porcentaje que representa muestra en total
    :param muestra: La cantidad dentro del total
    :param total: El total (el 100%)
    :return: El porcentaje, si total es != 0 o 0 en caso contrario
    """
    porcentaje = 0
    if total != 0:
        porcentaje = muestra * 100 / total

    return porcentaje


def es_digito(c):
    """
    Esta función revisa el caracter c y devuelve True
    en el caso de que corresponda a un dígito o False
    si no es así
    :param c: El caracter a revisar
    :return: True - Si c representa un dígito. False - En caso contrario
    """
    if '0' <= c <= '9':
        return True

    # Si llegó aquí, no es dígito
    return False


def es_vocal(c):
    """
    Comprueba si c es una vocal o no
    :param c: El caracter a revisar
    :return: True si el caracter es una vocal
    False en caso contraio
    """
    vocales = 'aeiouáéíóúAEIOUÁÉÍÓÚ'
    if c in vocales:
        return True

    return False


def es_consonante(car):
    """
    Comprueba si un caracter es consonante o no. En general no es necesaria una función
    de estas características, pero la dejo como referencia, tal como discutimos en clases.
    :param car: El caracter a comprobar
    :return: True si es consonante o False en caso contrario
    """
    car = car.lower()

    if 'a' <= car <= 'z' or car == 'ñ':
        if not es_vocal(car):
            return True

    return False


def principal():
    """
    Función principal del ejercicio
    :return: None
    """
    clp = cp = 0
    ter_car_pp = ''
    b_termina = b_empieza = False

    con_voc_en_pal = pos_ul_voc = 0
    con_pal_ter_car = con_pal_voc = con_pal_emp = 0
    texto = input('Ingrese texto (termina con "."): ')

    for car in texto:

        if car != ' ' and car != '.':
            # Aquí estamos dentro de la palabra
            clp += 1

            # Detectar la tercera letra de la primer palabra
            if cp == 0 and clp == 3:
                ter_car_pp = car

            # Si el caracter es igual al ter_car_pp
            if car == ter_car_pp:
                b_termina = True
            else:
                b_termina = False

            # Es vocal?
            if es_vocal(car):
                con_voc_en_pal += 1
                pos_ul_voc = clp
            elif es_consonante(car) and clp == 1:
                # No es realmente necesario preguntar si es consonante para este tipo de
                # ejercicios, lo hicimos en clases como para mostrar como sería.
                b_empieza = True

        else:
            # Aquí  puede haber termianado una palabra
            if clp > 0:
                # Aquí seguro terminó la palabra
                cp += 1

                # Termina con la TLPP?
                if b_termina:
                    con_pal_ter_car += 1

                if clp >= 5 and con_voc_en_pal > 0 and pos_ul_voc > clp // 2:
                    con_pal_voc += 1

                if clp % 2 == 1 and b_empieza:
                    con_pal_emp += 1

                # Reiniciamos las variables de UNA palabra
                pos_ul_voc = con_voc_en_pal = 0
                b_termina = b_empieza = False
                # Reiniciamos el CLP
                clp = 0

    print('==== RESULTADOS ====')
    print('Cantidad de palabras que terminan en la 3ra. letra de la primera palabra: ', con_pal_ter_car)
    print('Cantidad de palabras con vocales más allá de la mitad: ', con_pal_voc)
    print('Cantidad de palabras (longitud impar) que empiezan con consonante: ', con_pal_emp)
    porcentaje = calcular_porcentaje(con_pal_emp, cp)
    print('Representan un', round(porcentaje, 2), '% del total')


if __name__ == '__main__':
    principal()