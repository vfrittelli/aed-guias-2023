class Trabajo:
    def __init__(self, num, desc, tipo, imp, cant):
        self.numero = num
        self.descripcion = desc
        self.tipo = tipo
        self.importe = imp
        self.cantidad_personal = cant

    def __str__(self):
        r = "Numero: " + str(self.numero) + " Descripción: " + self.descripcion
        r += " Tipo: " + str(self.tipo) + " Importe: " + str(self.importe)
        r += " Cantidad de Personal: " + str(self.cantidad_personal)
        return r
