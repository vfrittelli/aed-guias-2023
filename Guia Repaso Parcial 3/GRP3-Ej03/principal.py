from random import randint, choice, uniform

import clase
from generales import validar_mayor_que, validar_rango


def menu():
    cadena = 'Menu de Opciones\n' \
             '============================================\n' \
             '1 ------ Cargar Arreglo de Ventas\n' \
             '2 ------ Mostrar los datos de las Ventas\n' \
             '3 ------ Mostrar Total Facturado por Perfume\n' \
             '4 ------ Mostrar Datos de Facturas por Rango\n' \
             '5 ------ Buscar Factura\n' \
             '0 ------ Salir\n' \
             'Ingrese su opcion: '
    return int(input(cadena))


def cargar_facturas(vector, n):
    for i in range(n):
        numero = f'{randint(1, 999999):0>8}'
        importe = uniform(1, 200000)
        tipo = choice(('A', 'B', 'C', 'D'))
        nombre = clase.crear_nombre()
        tipo_perf = randint(1, 17)
        factura = clase.Factura(numero, importe, tipo, nombre, tipo_perf)
        vector.append(factura)


def validar_tipo_factura(mensaje='Ingrese su opcion: ', *args):
    tipo = ''
    while tipo not in args:
        tipo = input(mensaje)
        if tipo not in args:
            print(f'El tipo de factura es invalido!!! Solo se permiten {args}')
    return tipo


def ordenar(vector):
    tam = len(vector)
    for i in range(tam - 1):
        for j in range(i + 1, tam):
            if vector[i].comprador < vector[j].comprador:
                vector[i], vector[j] = vector[j], vector[i]


def mostrar_facturas_filtradas(vector, x, t):
    ordenar(vector)
    sep = '-' * 75
    cadena = f'| {"Numero":<8} |  {"Importe":>10} | {"Tipo":^4} | {"Comprador":^30} | {"T Perf":<6} |\n{sep:<75}'
    print("{:<75}\n{:^75}\n{:<75}".format('=' * 75, 'Listado de Facturas', '=' * 75))
    print(cadena)
    for fact in vector:
        if fact.importe > x and fact.tipo != t:
            print(fact)


def acumular_por_tipo_perfume(vector):
    va = [0] * 17
    for fact in vector:
        va[fact.tipo_perfume - 1] += fact.importe
    return va


def mostrar_datos_por_rango_perfume(vector, tp_minimo, tp_maximo):
    print('Listado de datos de Facturas')
    for fact in vector:
        if tp_minimo <= fact.tipo_perfume <= tp_maximo:
            cad = f'Nro Factura {fact.numero} - Comprador: {fact.comprador} - Importe ${fact.importe:.2f}'
            print(cad)


def buscar(vector, m, p):
    pos = -1
    for i in range(len(vector)):
        if vector[i].numero == m and vector[i].importe < p:
            pos = i
            break
    return pos


def principal():
    opcion = -1
    facturas = []

    while opcion != 0:
        opcion = menu()
        if opcion == 1:
            n = validar_mayor_que(0, 'Ingrese la cantidad de facturas a cargar: ')
            cargar_facturas(facturas, n)

        if len(facturas) > 0:
            if opcion == 2:
                x = validar_rango(1, 200000, 'Ingrese el importe minimo a buscar: ')
                t = validar_tipo_factura('Ingrese el tipo de factura a excluir: ', 'A', 'B', 'C', 'D')
                mostrar_facturas_filtradas(facturas, x, t)

            elif opcion == 3:
                va = acumular_por_tipo_perfume(facturas)
                z = validar_rango(1, 17, 'Ingrese el tipo de perfume a mostrar: ')
                print(f'El monto total facturado para el tipo de perfume {z} es ${va[z - 1]:.2f}')

            elif opcion == 4:
                mostrar_datos_por_rango_perfume(facturas, 5, 12)

            elif opcion == 5:
                m = input('Ingrese factura a buscar: ')
                p = float(input('Ingrese el importe de la factura: '))
                pos = buscar(facturas, m, p)
                if pos != -1:
                    print(facturas[pos])
                else:
                    print('No existe la factura pedida')
        else:
            print('Primero debe generar las facturas con la opcion 1')


if __name__ == '__main__':
    principal()
