import random

class Factura:
    def __init__(self, numero, importe, tipo, comprador, tipo_perfume):
        self.numero = numero
        self.importe = importe
        self.tipo = tipo
        self.comprador = comprador
        self.tipo_perfume = tipo_perfume

    def __str__(self):
        sep = '-' * 75
        cadena = f'| {self.numero:<8} | ${self.importe:>10.2f} | ' \
                 f'{self.tipo:^4} | {self.comprador:<30} | {self.tipo_perfume:^6} |\n{sep:<75}'
        return cadena


def crear_nombre():
    nombres = 'Juan Socarron', 'Carlos Corredor', 'Laura Visor', 'Andrea Ovalo', 'Maria Andaluces'
    return random.choice(nombres)
