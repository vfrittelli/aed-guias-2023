# Guía Repaso Parcial 3

1. [Parcial 2019 - Servicio de Limpieza](GRP3-Ej01.md)
2. [Parcial 2021 - Empresa Agropecuaria](GRP3-Ej02.md)
3. [Parcial 2021 - La Perfumería](GRP3-Ej03.md)
4. [Parcial 2021 - Venta de Cruceros](GRP3-Ej04.md)
5. [Parcial 2020 - La Academia de Inglés](GRP3-Ej05.md)

