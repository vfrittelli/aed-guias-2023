from random import randint, uniform

import clase
from generales import validar_mayor_que, calcular_promedio


def menu():
    cadena = 'Menu de Opciones:\n' + \
             '===========================================\n' + \
             '1 - Cargar pasajes de cruceros \n' + \
             '2 - Mostrar datos ordenados por monto \n' + \
             '3 - Recaudacion acumulada por clase \n' + \
             '4 - Mostrar pasajes clase 3 con monto mayor al promedio\n' + \
             '5 - Buscar pasajero por pasaporte \n' + \
             '0 - Salir \n' + \
             'Ingrese su opcion: '
    return int(input(cadena))


def cargar_pasajes_cruceros(n):
    v = [None] * n
    for i in range(n):
        pasa = clase.crear_passaporte()
        nombre = clase.crear_nombre()
        destino = randint(100, 103)
        clas = randint(1, 10)
        monto = uniform(1500, 12500)
        v[i] = clase.Pasaje(pasa, nombre, destino, clas, monto)
    return v


def ordenar_pasajes(vector):
    tam = len(vector)
    for i in range(tam - 1):
        for j in range(i + 1, tam):
            if vector[i].monto > vector[j].monto:
                vector[i], vector[j] = vector[j], vector[i]


def mostrar(vector, titulo='Listado de Elementos'):
    print(titulo)
    print(clase.encabezado())
    for elem in vector:
        print(elem)
    print()


def acumular_por_clase(vector):
    va = [0] * 10
    for elem in vector:
        va[elem.clase - 1] += elem.monto
    return va


def calcular_promedio_clase(pasajes, clas):
    cantidad = 0
    total = 0
    for elem in pasajes:
        if elem.clase == clas:
            cantidad += 1
            total += elem.monto
    return calcular_promedio(total, cantidad)


def mayores_al_promedio(pasajes, promedio, clas):
    v = []
    for elem in pasajes:
        if elem.clase == clas and elem.monto > promedio:
            v.append(elem)
    return v


def buscar_pasaje(vector, pasaporte):
    pos = -1
    for i in range(len(vector)):
        if vector[i].pasaporte == pasaporte:
            pos = i
            break
    return pos


def buscar_clase_mayor_recaudacion(vector):
    mayor = vector[0]
    mayor_clase = 1
    for i in range(1, len(vector)):
        if vector[i] > mayor:
            mayor = vector[i]
            mayor_clase = i + 1
    return mayor_clase


def principal():
    opcion = -1
    pasajes = None

    while opcion != 0:
        opcion = menu()
        if opcion == 1:
            n = validar_mayor_que(0, 'Ingrese la cantidad de pasajes a cargar: ')
            pasajes = cargar_pasajes_cruceros(n)

        elif pasajes is not None and len(pasajes) > 0:
            if opcion == 2:
                ordenar_pasajes(pasajes)
                mostrar(pasajes, 'Listado de Pasajes Vendidos\n{}'.format('=' * 93))

            elif opcion == 3:
                va = acumular_por_clase(pasajes)
                mayor_clase = buscar_clase_mayor_recaudacion(va)
                for i in range(len(va)):
                    if va[i] > 0:
                        print('Clase {} recaudo un total ${:>10.2f}'.format(i + 1, va[i]))
                print('La clase con el mayor importe es', mayor_clase)

            elif opcion == 4:
                promedio_clase = calcular_promedio_clase(pasajes, 3)
                vec = mayores_al_promedio(pasajes, promedio_clase, 3)
                mostrar(vec, 'Listado de Pasajes de Clase 3\n{}'.format('=' * 93))

            elif opcion == 5:
                pasaporte = input('Ingresa el pasaporte a buscar: ')
                pos = buscar_pasaje(pasajes, pasaporte)
                if pos != -1:
                    print('Pasajero {}, por favor concurrir a atención al cliente'.format(pasajes[pos].nombre))
                else:
                    print('No hay ningun pasaje vendido para el pasaporte {}'.format(pasaporte))
        else:
            print('Primero debe ejecutar la opcion 1')


if __name__ == '__main__':
    principal()
