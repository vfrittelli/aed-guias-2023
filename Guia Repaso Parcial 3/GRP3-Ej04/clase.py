import string
import random

class Pasaje:
    def __init__(self, pasaporte, nombre, destino, clase, monto):
        self.pasaporte = pasaporte
        self.nombre = nombre
        self.destino = destino
        self.clase = clase
        self.monto = monto

    def __str__(self):
        cadena = '| {:<10} | {:<30} | {:<21} | {:<5} | ${:>10.2f} |\n{:<92}'
        nombre_destino = to_descripcion_destino(self.destino)
        return cadena.format(self.pasaporte, self.nombre, nombre_destino, self.clase, self.monto, '-' * 93)


def to_descripcion_destino(destino):
    destinos = ('Bahamas', 'Bermudas', 'Puerto Rico', 'República Dominicana')
    pos = destino - 100
    return destinos[pos]


def encabezado():
    cadena = '| {:<10} | {:^30} | {:^21} | {:<5} | {:>11} |\n{:<76}'
    return cadena.format('Pasaporte', 'Nombre', 'Destino', 'Clase', 'Monto', '=' * 93)


def crear_passaporte():
    cadena = string.ascii_uppercase + string.digits
    pasaporte = ''
    for i in range(10):
        pasaporte += random.choice(cadena)
    return pasaporte


def crear_nombre():
    nombres = ('Juan', 'Carlos', 'Andres', 'Karina', 'Marcela', 'Irina', 'Laura', 'Joaquin', 'Ignacio')
    apellidos = ('Peres', 'Perez', 'Santorini', 'Salerno', 'Vineciano', 'Andaluz', 'Macarron', 'Socarron')
    return '{} {}'.format(random.choice(nombres), random.choice(apellidos))
