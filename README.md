# AED Guías 2023

Repositorio con las guías de ejercicios y sus soluciones para el ciclo lectivo 2023


## Listado de Guías

- [Guía 01 - Fundamentos de Programación](./Guia%2001)
- [Guía 02 - Estructuras Secuenciales](./Guia%2002)
- [Guía 03 - Tipos estructurados Básicos](./Guia%2003)
- [Guía 04 - Estructuras Condicionales](./Guia%2004)
- [Guía 05 - Estructuras Condicionales - Variantes](./Guia%2005)
- [Guía 06 - Estructuras Repetitivas: Ciclo while](./Guia%2006)
- [Guía 07 - Estructuras Repetitivas: Ciclo for](./Guia%2007)
- [Guía 08 - Estructuras Repetitivas: Ciclo for y Ejercicios Parcial I](./Guia%2008)
- [Guía 09 - Estructuras Repetitivas: Ciclo do while y Ejercios Parcial 1](./Guia%2009)
- [Guía 10 - Procesamiento de Secuencias de Caracteres](./Guia%2010)
- [Guía 11 - Procesamiento de Secuencias de Caracteres 2](./Guia%2011) 
- [Guía 12 - Programación Modular](./Guia%2012)
- [Guia 13 - Módulos y Paquetes](./Guia%2013) 
- [Guia 14 - Recursividad y Simulacros Parcial 2](./Guia%2014)
- [Guia 15 - Arreglos Unidimensionales](./Guia%2015)
- [Guia 16 - Ordenamiento y Búsqueda](./Guia%2016)
- [Guia 17 - Arreglos: Casos de Estudio I](./Guia%2017)
- [Guia 18 - Arreglos: Casos de Estudio II](./Guia%2018)
- [Guia 19 - Registros y Objetos](./Guia%2019)
- [Guia 20 - Arreglos de Objetos](./Guia%2020)
- [Guia 21 - Arreglos Bidimensionales](./Guia%2021)
- [Guia 22 - Ordenamiento](./Guia%2022)
- [Guia 23 - Archivos](./Guia%2023)
- [Guía 24 - Archivos de Texto](./Guia%2024)
- [Guía 25 - Archivos Aplicaciones Prácticas](./Guia%2025)
- [Guía 26 - Archivos Gestión ABM](./Guia%2026)
- [Guía 27 - Preparación Parcial 4](./Guia%2027)
- [Guia Repaso Parcial 2](./Guia%20Repaso%20Parcial%202)
- [Guia Repaso Parcial 3](./Guia%20Repaso%20Parcial%203)
- [Guia Repaso Parcial 4](./Guia%20Repaso%20Parcial%204)
