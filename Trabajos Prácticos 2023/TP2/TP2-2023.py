import os.path


def country(pat):
    lp = len(pat)

    # ...si tiene menos de 6 o más de 7 caracteres, es otro país...
    if lp < 6 or lp > 7:
        return 'Otro'

    # ...si tiene 6 caracteres, es de Chile o es de otro país con 6 caracteres...
    if lp == 6:
        if pat[0:4].isalpha() and pat[4:6].isdigit():
            return 'Chile'
        else:
            return 'Otro'

    # ...si llegó hasta acá, tiene 7 caracteres...
    if pat[0:2].isalpha() and pat[2:5].isdigit() and pat[5:7].isalpha():
        return 'Argentina'

    if pat[0:2].isalpha() and pat[2:7].isdigit():
        return 'Bolivia'

    if pat[0:3].isalpha() and pat[3].isdigit() and pat[4].isalpha() and pat[5:7].isdigit():
        return 'Brasil'

    if pat[0:4].isalpha() and pat[4:7].isdigit():
        return 'Paraguay'

    if pat[0:3].isalpha() and pat[3:7].isdigit():
        return 'Uruguay'

    # ...si ninguno fue cierto, entonces es otro...
    return 'Otro'


def final_amount(pais, tipo, pago):
    # Determinación del importe básico a pagar...
    # ...monto base para Bolivia...
    if pais == 1:
        base = 200

    # ...monto base para Brasil...
    elif pais == 2:
        base = 400

    # ...monto base para todos los demás...
    else:
        base = 300

    # ...asumimos que es un auto...
    basico = base

    # ... y si no lo fuese, alguna de las que siguen será cierta...
    if tipo == 0:
        # ...es una moto...
        basico = base // 2

    elif tipo == 2:
        # ...es un camión...
        basico = int(1.6 * base)

    # Determinación del valor final del ticket a pagar...
    # ...asumimos que es pago manual...
    final = basico

    # ...y si no lo fuese, la siguiente será cierta y cambiará el valor...
    if pago == 2:
        final = int(0.9 * basico)

    return final


def principal():
    # Inicialización de variables auxiliares...
    # ...contadores para Resultado 2...
    carg = cbol = cbra = cchi = cpar = curu = cotr = 0

    # ...acumulador para Resultado 3...
    imp_acu_total = 0

    # ...flag y contador para Resultado 4...
    first, cpp, primera = True, 0, ''

    # ...variables para el mayor importe final para Resultado 5...
    mayimp, maypat = 0, ''

    # ...contador de líneas leídas para Resultado 6...
    cl = 0

    # ...acumulador y contador para Resultado 7...
    da = cv = 0

    # Títulos y presentación...
    # print("AED - Trabajo Práctico 2 - Solución Sugerida")

    # Nombre del archivo de texto de entrada...
    # ... se asume que está en la misma carpeta del proyecto...
    fd = 'peajes.txt'

    # Control de existencia...
    if not os.path.exists(fd):
        print('El archivo', fd, 'no existe...')
        print('Revise, y reinicie el programa...')
        exit(1)

    # Procesamiento del archivo de entrada...
    # print('Procesando el archivo', fd, '...')

    # Apertura del archivo...
    m = open(fd, 'rt')

    # Procesamento de la línea de timestamp...
    # Resultado 1...
    line = m.readline()
    idioma = 'Español'
    if 'PT' in line:
        idioma = 'Portugués'

    # Procesamiento de los peajes registrados...
    while True:
        # ...intentar leer la linea que sigue...
        line = m.readline()

        # ...si se obtuvo una cadena vacia, cortar el ciclo y terminar...
        if line == '':
            break

        # ...procesar la línea leída si el ciclo no cortó...
        # ... obtener cada dato por separado, y en el tipo correcto...
        patente = line[0:7].lstrip()
        tipo = int(line[7])
        pago = int(line[8])
        pais = int(line[9])
        distancia = int(line[10:13])

        # ... determinar el país de origen de la patente...
        pais_origen = country(patente)

        # Resultado 2: contar patentes por país...
        if pais_origen == 'Argentina':
            carg += 1

        elif pais_origen == 'Bolivia':
            cbol += 1

        elif pais_origen == 'Brasil':
            cbra += 1

        elif pais_origen == 'Chile':
            cchi += 1

        elif pais_origen == 'Paraguay':
            cpar += 1

        elif pais_origen == 'Uruguay':
            curu += 1

        else:
            cotr += 1

        # Importe final a pagar en ese ticket...
        final = final_amount(pais, tipo, pago)

        # Resultado 3: Acumulación de los importes finales...
        imp_acu_total += final

        # Resultado 4...
        # ...detectar primera patente leída...
        if first is True:
            primera = patente
            first = False

        # ...contar cuántas veces apareció la primera patente...
        if patente == primera:
            cpp += 1

        # Resultado 5...
        if mayimp == 0 or final > mayimp:
            mayimp = final
            maypat = patente

        # Resultado 6...
        # ... simplemente contar la línea leída, para hacer porcentaje al final...
        cl += 1

        # Resultado 7...
        # ...contar vehículos de Argentina que pasaron por cabinas brasileñas...
        if pais_origen == 'Argentina' and pais == 2:
            da += distancia
            cv += 1

    # Cierre del archivo...
    m.close()

    # Resultado 6...
    porc = 0
    if cl != 0:
        porc = round(cotr * 100 / cl, 2)

    # Resultado 7...
    prom = 0
    if cv != 0:
        prom = round(da / cv, 2)

    # Visualización de resultados...
    print()
    print('(r1) - Idioma a usar en los informes:', idioma)

    print()
    print('(r2) - Cantidad de patentes de Argentina:', carg)
    print('(r2) - Cantidad de patentes de Bolivia:', cbol)
    print('(r2) - Cantidad de patentes de Brasil:', cbra)
    print('(r2) - Cantidad de patentes de Chile:', cchi)
    print('(r2) - Cantidad de patentes de Paraguay:', cpar)
    print('(r2) - Cantidad de patentes de Uruguay:', curu)
    print('(r2) - Cantidad de patentes de otro país:', cotr)

    print()
    print('(r3) - Importe acumulado total de importes finales:', imp_acu_total)

    print()
    print('(r4) - Primera patente del archivo:', primera, '- Frecuencia de aparición:', cpp)

    print()
    print('(r5) - Mayor importe final cobrado:', mayimp, '- Patente a la que se cobró ese importe:', maypat)

    print()
    print('(r6) - Porcentaje de patentes de otros países:', porc, '\b%')

    print()
    print('(r7) - Distancia promedio recorrida por vehículos argentinos pasando por cabinas brasileñas:', prom, '\bkm')


# script principal...
if __name__ == '__main__':
    principal()