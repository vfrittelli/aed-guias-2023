class Ticket:
    def __init__(self, codigo, patente, tipo, forma_de_pago, pais_cabina, km_recorridos ):
        self.codigo = codigo
        self.patente = patente
        self.tipo = tipo
        self.forma_de_pago = forma_de_pago
        self.pais_cabina = pais_cabina
        self.km_recorridos = km_recorridos

    def __str__(self):
        tipos_vehiculo = ("Motocicleta", "Automóvil", "Camión")
        formas_pago = ("Manual", "Telepeaje")
        cabinas = ("Argentina", "Bolivia", "Brasil", "Paraguay", "Uruguay")

        cad = 'Código: {:<15} | Patente: {:<10} | Tipo: {:<15} | Forma pago: {:<10} | Cabina: {:<15} | Distancia: {:>5}km'
        cad = cad.format(self.codigo, self.patente, tipos_vehiculo[self.tipo], formas_pago[self.forma_de_pago - 1], cabinas[self.pais_cabina], self.km_recorridos)
        return cad


def validar_rango(inf, sup, msj):
    valor = int(input(msj))
    while sup < valor < inf:
        print("El valor ingresado no es correcto. Intente nuevamente.")
        valor = input(msj)
    return valor


# MENÚ DE OPCIONES
def menu():
    print("-=" * 300)
    print("MENÚ DE OPCIONES")
    print("-=" * 300)
    print("1. Cargar arreglo de objetos desde el archivo")
    print("2. Cargar objeto individual por teclado")
    print("3. Mostrar listado de objetos ordenados por código de menor a mayor")
    print("4. Buscar objeto por patente y por país de cabina utilizada")
    print("5. Buscar objeto por codigo de ticket emitido")
    print("6. Generar listado de cantidad de vehículos por pais")
    print("7. Mostrar importe acumulado por cada tipo de vehículo procesado")
    print("8. Buscar el tipo de vehículo con mayor monto acumulado y mostrar monto acumulado")
    print("9. Mostrar distancia promedio y cantidad de vehiculos que recorrieron más de esa distancia")
    print("10. Salir")
    print("-=" * 300)
    op = int(input("Ingrese una opción: "))
    return op
