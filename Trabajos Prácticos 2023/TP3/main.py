from ticket import *


# OPCIÓN 1
def cargar_arreglo(v):
    primera = False
    archivo = open("peajes-tp3.txt", 'rt')
    tickets = archivo.readlines()

    for ticket in tickets:
        if not primera:
            primera = True

        else:
            linea = ticket.strip()
            codigo = linea[:10]
            patente = linea[10:17].strip()
            tipo = int(linea[17])
            forma_de_pago = int(linea[18])
            pais_cabina = int(linea[19])
            km_recorridos = int(linea[20:])
            nuevo_ticket = Ticket(codigo, patente, tipo, forma_de_pago, pais_cabina, km_recorridos)
            v.append(nuevo_ticket)

    archivo.close()


# OPCIÓN 2
def cargar_ticket_individual(v):
    codigo = str(input("Ingresar el código del ticket: "))
    patente = input("Ingresar la patente del vehículo: ")
    tipo = validar_rango(0, 2, "Ingresar el tipo de vehiculo (0, 1 o 2) : ")
    forma_de_pago = validar_rango(1, 2, "Ingresar la forma de pago (1 o 2): ")
    pais_cabina = validar_rango(0, 4, "Ingresar el pais de la cabina (entre 0 y 4): ")
    km_recorridos = validar_rango(0, 1000, "Ingresar la cantidad de kilometros recorridos: ")
    ticket = Ticket(codigo, patente, tipo, forma_de_pago, pais_cabina, km_recorridos)
    v.append(ticket)


# OPCIÓN 3
def ordenar_tickets(v):
    n = len(v)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if v[i].codigo > v[j].codigo:
                v[i], v[j] = v[j], v[i]


def mostrar(v):
    nombres_paises = ("Argentina", "Bolivia", "Brasil", "Paraguay", "Uruguay", "Chile", "Otro", )
    for ticket in v:
        pais = pais_identificado(ticket.patente)
        print(ticket,  " | PAIS DE LA PATENTE:", nombres_paises[pais])


# OPCIÓN 4
def buscar_vehiculo_patente_cabina(v):
    patente = input("Ingresar la patente del vehículo: ")
    cabina = int(input("Ingresar el numero del pais de la cabina por la cual pasó el vehículo (0-4): "))

    for ticket in v:
        if ticket.patente == patente and ticket.pais_cabina == cabina:
            print("Se encontró")
            print(ticket)
            return
    print("El vehiculo buscado NO se encontró")


# OPCIÓN 5
def buscar_ticket_codigo(v):
    codigo = input("Ingresar el código del ticket: ")
    for ticket in v:
        if ticket.codigo == codigo:
            print("TICKET BUSCADO")
            print(ticket)
            if ticket.forma_de_pago == 1:
                ticket.forma_de_pago = 2
            else:
                ticket.forma_de_pago = 1
            print("TICKET CON FORMA DE PAGO MODIFICADA")
            print(ticket)
            return
    print("No se encontró el ticket buscado")


# OPCIÓN 6
def pais_identificado(pat):
    lp = len(pat)

    # ...si tiene menos de 6 o más de 7 caracteres, es otro pais...
    if lp < 6 or lp > 7:
        return 'Otro'

    # ...si tiene 6 caracteres, es de Chile o es de otro pais...
    if lp == 6:
        # ...Chile...
        if pat[0:4].isalpha() and pat[4:6].isdigit():
            return 5

        # ...Otro...
        else:
            return 6

    # ...si llegó hasta acá, tiene 7 caracteres...
    # ...Argentina...
    if pat[0:2].isalpha() and pat[2:5].isdigit() and pat[5:7].isalpha():
        return 0

    # ...Bolivia...
    if pat[0:2].isalpha() and pat[2:7].isdigit():
        return 1

    # ...Brasil...
    if pat[0:3].isalpha() and pat[3].isdigit() and pat[4].isalpha() and pat[5:7].isdigit():
        return 2

    # ...Paraguay...
    if pat[0:4].isalpha() and pat[4:7].isdigit():
        return 3

    # ...Uruguay...
    if pat[0:3].isalpha() and pat[3:7].isdigit():
        return 4

    # ...si ninguno fue cierto, entonces es Otro...
    return 6


def contar_paises(paises, v):
    for ticket in v:
        pais = pais_identificado(ticket.patente)
        paises[pais] += 1


def mostrar_contador(contador_paises):
    nombres_paises = ("Argentina", "Bolivia", "Brasil", "Paraguay", "Uruguay", "Chile", "Otro")
    for i in range(len(contador_paises)):
        print("Pais del vehiculo:", nombres_paises[i], "| Cantidad de autos procesados:", contador_paises[i])


# OPCIÓN 7
def calcular_importe_final(v):
    # Defino el importe base
    if v.pais_cabina == 2:
        importe_base = 400
    elif v.pais_cabina == 1:
        importe_base = 200
    else:
        importe_base = 300

    # Defino el importe basico
    if v.tipo == 0:
        importe_basico = importe_base * 0.5
    elif v.tipo == 2:
        importe_basico = importe_base * 1.6
    else:
        importe_basico = importe_base

    # Defino el importe final
    if v.forma_de_pago == 2:
        importe_final = importe_basico * 0.9
    else:
        importe_final = importe_basico

    return importe_final


def acumular_importes_finales(v, tipos_vehiculos):
    n = len(v)
    for i in range(n):
        importe_final = calcular_importe_final(v[i])
        tipos_vehiculos[v[i].tipo] += importe_final


def mostrar_acumulador(tipos_vehiculos):
    nombres_vehiculos = ("Motocicleta", "Automóvil", "Camión")
    n = len(tipos_vehiculos)
    for i in range(n):
        print("Tipo de vehículo:", nombres_vehiculos[i], "| Importes finales acumulados:", tipos_vehiculos[i])


# OPCIÓN 8
def buscar_mayor(tipos_vehiculos):
    nombres_vehiculos = ("Motocicleta", "Automóvil", "Camión")
    indice = 0
    tipo_mayor = 0
    monto_acumulado = 0
    primer_mayor = False
    n = len(tipos_vehiculos)
    for i in range(n):
        if not primer_mayor:
            tipo_mayor = tipos_vehiculos[i]
            indice = i
            primer_mayor = True
        elif tipos_vehiculos[i] > tipo_mayor:
            tipo_mayor = tipos_vehiculos[i]
            indice = i
        monto_acumulado += tipos_vehiculos[i]
    print("-=" * 300)
    print("El tipo de vehiculo con mayor monto acumulado es el tipo:", nombres_vehiculos[indice])
    print("Porcentaje sobre el monto total:", round((tipo_mayor * 100) / monto_acumulado, 2), "\b%")


# OPCIÓN 9
def acumular_distancias(v):
    distancia, cantidad_vehiculos = 0, 0
    for ticket in v:
        distancia += ticket.km_recorridos
        cantidad_vehiculos += 1
    return distancia, cantidad_vehiculos


def vehiculos_may_promedio(v, promedio):
    cant_vehiculos = 0
    for ticket in v:
        if ticket.km_recorridos > promedio:
            cant_vehiculos += 1
    print(cant_vehiculos, "recorrieron una distancia mayor al promedio")


# FUNCIÓN DE ENTRADA
def principal():
    op = -1
    v = []
    ban_op7 = False
    while op != 10:
        op = menu()

        if op == 1:
            if len(v) != 0:
                print("Esta a punto de borrar los datos del arreglo. Desea continuar?")
                respuesta = int(input("Presione 1 si desea continuar, presione 2 (u otro) si desea cancelar: "))
                if respuesta == 1:
                    v = []
                    cargar_arreglo(v)
                    print("El arreglo fue cargado nuevamente, con exito.")
            else:
                cargar_arreglo(v)
                print("El arreglo fue cargado con exito.")
            print()

        elif op == 2:
            cargar_ticket_individual(v)
            print("El ticket fue agregado con exito al arreglo.")
            print()

        elif op == 3:
            if len(v) != 0:
                ordenar_tickets(v)
                mostrar(v)
            else:
                print("Debe cargar el arreglo primero...")
            print()

        elif op == 4:
            if len(v) != 0:
                buscar_vehiculo_patente_cabina(v)
            else:
                print("Debe cargar el arreglo primero...")
            print()

        elif op == 5:
            if len(v) != 0:
                buscar_ticket_codigo(v)
            else:
                print("Debe cargar el arreglo primero...")
            print()

        elif op == 6:
            if len(v) != 0:
                contador_paises = [0] * 7
                contar_paises(contador_paises, v)
                mostrar_contador(contador_paises)
            else:
                print("Debe cargar el arreglo primero...")
            print()

        elif op == 7:
            if len(v) != 0:
                ban_op7 = True
                tipos_vehiculos = [0] * 3
                acumular_importes_finales(v, tipos_vehiculos)
                mostrar_acumulador(tipos_vehiculos)
            else:
                print("Debe cargar el arreglo primero...")
            print()

        elif op == 8:
            if ban_op7:
                buscar_mayor(tipos_vehiculos)
            else:
                print("Antes de ejecutar esta opción, debe ejecutar la opción 7.")
            print()

        elif op == 9:
            if len(v) != 0:
                distancia, cantidad_vehiculos = acumular_distancias(v)
                promedio = round(distancia / cantidad_vehiculos, 2)
                print("La distancia promedio recorrida por los vehiculos es: ", promedio)
                vehiculos_may_promedio(v, promedio)
            else:
                print("Debe cargar el arreglo primero...")
            print()

        elif op == 10:
            print("-=" * 300)
            print("Adios!")
            print("PEAJE CERRADO")
            print("-=" * 300)


if __name__ == '__main__':
    principal()
