# AED - TP1 2023 - Solución sugerida.
# Esta solución sugerida contempla solo elementos contenidos
# entre las Fichas 01 y 05. La solución también se basa en
# intentar deducir de qué país es una patente en base al
# análisis lógico de subgrupos de caracteres, en lugar de
# simplemente hacer cinco condiciones (una por cada país posible)
# en las que se pregunte por cada una de las siete posiciones
# (eso esto también es válido, pero cada condición requeriría
# siete proposiciones encadenadas con and).
# Otras soluciones basadas en técnicas
# más avanzadas, son obviamente también válidas.

# Títulos y carga de datos.
print("Playa de estacionamiento - Gestión de egresos")
patente = input("Ingrese la patente del vehículo que egresa: ")
tipo = int(input("Tipo de vehículo (0: moto - 1: auto - 2: camión): "))
pago = int(input("Forma de pago (1: manual - 2: telepeaje): "))
pais = int(input("País de la cabina (0: Argentina - 1: Bolivia - 2: Brasil - 3: Paraguay - 4: Uruguay): "))
distancia = float(input("Distancia desde la última cabina (o cero si esta es la primera): "))

# Procesos.
# 1. Determinación del país del origen del vehículo.
if len(patente) != 7:
    origen = 'Otro'

else:
    if 'A' <= patente[0] <= 'Z' and 'A' <= patente[1] <= 'Z':
        if '0' <= patente[2] <= '9':
            # el origen es Argentina o es Bolivia...
            if '0' <= patente[3] <= '9' and '0' <= patente[4] <= '9':
                if '0' <= patente[5] <= '9' and '0' <= patente[6] <= '9':
                    origen = 'Bolivia'

                else:
                    if 'A' <= patente[5] <= 'Z' and 'A' <= patente[6] <= 'Z':
                        origen = 'Argentina'
                    else:
                        origen = 'Otro'
            else:
                origen = 'Otro'

        else:
            # el origen es Brasil, Paraguay o Uruguay...
            if 'A' <= patente[3] <= 'Z':
                if '0' <= patente[4] <= '9' and '0' <= patente[5] <= '9' and '0' <= patente[6] <= '9':
                    origen = 'Paraguay'
                else:
                    origen = 'Otro'

            else:
                if '0' <= patente[4] <= '9':
                    if '0' <= patente[5] <= '9' and '0' <= patente[6] <= '9':
                        origen = 'Uruguay'
                    else:
                        origen = 'Otro'

                else:
                    if '0' <= patente[5] <= '9' and '0' <= patente[6] <= '9':
                        origen = 'Brasil'
                    else:
                        origen = 'Otro'

    else:
        origen = 'Otro'

# 2. Determinación del importe básico a pagar.
# monto base para Bolivia...
if pais == 1:
    base = 200

# monto base para Brasil...
elif pais == 2:
    base = 400

#  monto base para todos los demás...
else:
    base = 300

# asumimos que es un auto...
basico = base

# ... y si no lo fuese, alguna de las que siguen será cierta...
if tipo == 0:
    # es una moto...
    basico = base // 2

elif tipo == 2:
    # es un camión...
    basico = int(1.6 * base)

# 3. Determinación del valor final del ticket a pagar.
# asumimos que es pago manual...
final = basico

# ... y si no lo fuese, la siguiente será cierta y cambiará el valor...
if pago == 2:
    final = int(0.9 * basico)

# 4. Determinación del importe promedio por kilómetro recorrido.
if distancia != 0:
    promedio = round(final / distancia, 2)
    salida = str(promedio)
else:
    salida = "No aplica (esta es la primera cabina)"

# Visualización de resultados.
print()
print("TICKET DE EGRESO")
print("País de origen del vehículo:", origen)
print("Importe básico a pagar:", basico)
print("Importe final a pagar:", final)
print("Importe promedio por kilómetro desde la última cabina:", salida)
